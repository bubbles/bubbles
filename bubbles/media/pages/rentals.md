# Rentals

At the core of Bubbles is the rental system. This allows members to rent out
equipment that Bubbles manages. A rental has a life-cycle of three stages:

 1. The rental is *requested* by a member
 1. The rental is then *rented* once equipment is assigned to the member and
    they have taken posession of it.
 1. Once the member returns the equipment, the rental is considered *returned*.

Typically, a member [requests equipment](request_equipment) for their own
account, which then is displayed in the **Rental Queue** on the Admin screen. An
admin can also [add a rental](/admin/rentals/add/)
